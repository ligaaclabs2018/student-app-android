package labs.waterford.org.studentapp.services;


import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if(notification != null){
            Log.d(TAG, "Notif: " + notification.getBody());
            Log.d(TAG, "Title: " + notification.getTitle());
        }
        Map<String, String> data = remoteMessage.getData();
        for (String s : data.keySet()) {
            Log.d(TAG, s  + " - " + data.get(s));
        }
    }
}
