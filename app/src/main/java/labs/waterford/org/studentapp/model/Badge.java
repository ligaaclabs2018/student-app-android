package labs.waterford.org.studentapp.model;

import java.util.Date;

/**
 * Created by mariuscaciulescu on 3/18/18.
 */

public class Badge {

    private String imageUrl;
    private String text;
    private Date date;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Badge{" +
                "imageUrl='" + imageUrl + '\'' +
                ", text='" + text + '\'' +
                ", date=" + date +
                '}';
    }
}
