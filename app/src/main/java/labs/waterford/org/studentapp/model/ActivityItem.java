package labs.waterford.org.studentapp.model;

import java.util.Date;

/**
 * Created by mariuscaciulescu on 3/18/18.
 */

public class ActivityItem {

    private String subject;

    private double grade;

    private Date date;

    private boolean absent;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isAbsent() {
        return absent;
    }

    public void setAbsent(boolean absent) {
        this.absent = absent;
    }

    @Override
    public String toString() {
        return "ActivityItem{" +
                "subject='" + subject + '\'' +
                ", grade=" + grade +
                ", date=" + date +
                ", absent=" + absent +
                '}';
    }
}
