package labs.waterford.org.studentapp;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;

import java.util.Timer;
import java.util.TimerTask;

import labs.waterford.org.studentapp.util.AppHelper;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    private Timer myTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AWSMobileClient.getInstance().initialize(this).execute();
        AppHelper.init(getApplicationContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                CognitoUser currentUser = AppHelper.getPool().getCurrentUser();
                currentUser.getSession(new AuthenticationHandler() {
                    @Override
                    public void onSuccess(CognitoUserSession userSession, CognitoDevice newDevice) {
                        goToMain();
                    }

                    @Override
                    public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String userId) {
                        goToLogin();
                    }

                    @Override
                    public void getMFACode(MultiFactorAuthenticationContinuation continuation) {
                        goToLogin();
                    }

                    @Override
                    public void authenticationChallenge(ChallengeContinuation continuation) {
                        goToLogin();
                    }

                    @Override
                    public void onFailure(Exception exception) {
                        goToLogin();
                    }
                });
            }
        }, 1000, 1000);
    }

    private void goToLogin() {
        myTimer.cancel();
        this.runOnUiThread(goToLogin);
    }

    private void goToMain() {
        myTimer.cancel();
        this.runOnUiThread(goToMain);
    }

    private Runnable goToLogin = new Runnable() {
        public void run() {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    };

    private Runnable goToMain = new Runnable() {
        public void run() {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    };
}
